package com.lifesensor.walltree;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

public class BluetoothClient {

    private static final String TAG = "BLUETOOTHCLIENT";
    public static final int REQUEST_ENABLE_BT = 100;

    private Context context;
    private BluetoothAdapter adapter;
    private BluetoothSocket socket;
    private InputStream i;
    private OutputStream o;
    private static boolean isConnectionSuccess;
    private static boolean finished;

    private BluetoothListener listener;

    public interface BluetoothListener {
        void communicationBreakdown();
    }

    /**
     * Constructor
     * @param context the context of the Activity
     */
    public BluetoothClient(Context context){
        this.context = context;
        adapter = BluetoothAdapter.getDefaultAdapter();

    }

    public boolean startDiscovery(){
        Log.d(TAG,"Starting discovery...");
        if(adapter.isEnabled()) {
            adapter.startDiscovery();
            Log.d(TAG,"Discovery started...");
            return true;
        }

        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        ((Activity) context).startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        return false;
    }

    public void stopDiscovery(){
        if(adapter.isEnabled()) {
            adapter.cancelDiscovery();
            Log.d(TAG,"Discovery canceled...");
        }
    }

    public void list(){
        Set<BluetoothDevice> pairedDevices = adapter.getBondedDevices();

        if (pairedDevices.size() > 0) {
            // There are paired devices. Get the name and address of each paired device.
            for (BluetoothDevice device : pairedDevices) {
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC address
                Log.d(TAG, String.format("list: hw: %s; nm: %s", deviceHardwareAddress, deviceName));
            }
        }

    }

    /**
     * Blocking function. It blocks discovery operation, if any.
     */
    private void discardPreviousDiscovering(){
        Log.e("discard","Check previous disconvering...");
        if(adapter.isDiscovering())
            adapter.cancelDiscovery();
        while (adapter.isDiscovering());
        Log.e("discard","Disconvering: " + String.valueOf(adapter.isDiscovering()));
    }

    /**
     * Blocking function. It activates the bluetooth
     */
    private void activateBluetooth() {
        Log.e("ActBT", String.valueOf(adapter.isEnabled()));
        if(!adapter.isEnabled()) {
            adapter.enable();
            while (!adapter.isEnabled());
        }
        Log.e("ActBT", String.valueOf(adapter.isEnabled()));
    }

    /**
     * Release all the opened resources
     */
    public void closeAll() {
        if (i != null) {
            try {
                i.close();
            } catch (IOException e) {
                handleException("closeAll", e);
            }
        }

        if (o != null) {
            try {
                o.close();
            } catch (IOException e) {
                handleException("closeAll", e);
            }
        }

        if (socket != null) {
            try {
                socket.close();
            } catch (IOException e) {
                handleException("closeAll", e);
            }
        }

        if (adapter != null) {
            adapter.disable();
            while (adapter.isEnabled());
        }

        Log.d("closeAll", "closed");
    }

    /**
     * Function used to catch an exception
     * @param tag the identifier of the log in Logcat
     * @param e the exceotion raised
     */
    private void handleException(String tag, Exception e) {
        Log.e(tag, e.getMessage());
        e.printStackTrace();
    }

    public void checkStatus(String s){
        if(s.length() == 0){
            Toast.makeText(context, "Inserire un \"Remote name\" per far avviare la scansione", Toast.LENGTH_LONG).show();
            return;
        }

        Log.d(TAG, "checkStatus");
        Log.d(TAG, "checkStatus: enabled: " + adapter.isEnabled());
        Log.d(TAG, "checkStatus: discovering: " + adapter.isDiscovering());
        if(!adapter.isDiscovering())
            startDiscovery();
    }

    /**
     *
     * @param address the bluetooth address of the tracker; format XX:XX:XX:XX:XX:XX
     * @param key   Base64 string. This is the key used to authenticate with the IoT Hub
     * @param member member-id
     * @param tracker kid or elderly
     */
    private int attempt = 1;
    public void connect(String address, String key, String member, String tracker) throws IllegalArgumentException {
        validate(address, key, member, tracker);

        attempt = 1;
        do {

            Log.e(TAG, "Tentativo "+ attempt + " di connessione");
            try {
                BluetoothDevice d = null;
                Log.i("MAIN", "waiting for a device to show...");
                //Connection to the bluetooth server socket
                while (d == null) {
                    try {
                        d = adapter.getRemoteDevice(address);
                        d.setPin(new byte[]{0, 0, 0, 1});
                        socket = d.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00000003-0000-1000-8000-00805f9b34fb"));
                    } catch (Exception e) {
                        Log.e(TAG, "Still no device");
                        d = null;
                        socket = null;
                    }
                }
                socket.connect();

                //construcion of the JSON that will be sent
                JSONObject j = new JSONObject();
                j.put("key", key);
                j.put("member_id", member);
                j.put("tracker_type", tracker);
                Log.d(TAG, "Sent: " + j.toString());
                o = socket.getOutputStream();
                o.write(j.toString().getBytes());

                //waiting for the response from the server
                i = socket.getInputStream();
                int l = i.available();
                while (l <= 0) {
                    l = i.available();
                }

                byte[] bytes = new byte[l];
                i.read(bytes);
                String response = new String(bytes);
                Log.e("READ", response);

                JSONObject r = new JSONObject(response);
                if (r.has("error"))
                    isConnectionSuccess = r.getString("error").equals("ok");

            } catch (Exception e) {
                handleException("create", e);

            }

            attempt += 1;

        } while(!isConnectionSuccess && attempt < 5);

        finished = true;
        closeAll();

        if(listener != null)
            listener.communicationBreakdown();
    }

    public void setListener(BluetoothListener listener){
        this.listener = listener;
    }

    public boolean isConnectionSuccess() {
        return isConnectionSuccess;
    }

    public boolean isFinished() {
        return finished;
    }

    public int getAttempt() {
        return attempt;
    }

    /**
     * Check if the parameters have the correct format
     * @param address   the bluetooth address of the tracker; format XX:XX:XX:XX:XX:XX
     * @param key       Base64 string. This is the key used to authenticate with the IoT Hub
     * @param member    member-id
     * @param tracker   kid or elderly
     */
    private void validate(String address, String key, String member, String tracker) throws IllegalArgumentException{
        if(!validateAddress(address))
            throw new IllegalArgumentException("Address has wrong format");
        if(!isBase64(key))
            throw new IllegalArgumentException("Key is not in Base64");
        if(!validateTracker(tracker))
            throw new IllegalArgumentException("Expected values for tracker: 'kid' or 'elderly'");

    }

    /**
     * Check the format of the address
     * @param address the bluetooth address of the tracker; format XX:XX:XX:XX:XX:XX
     * @return true if the address has the correct format; false otherwise;
     */
    private boolean validateAddress(String address){
        if(address.length()!=17)
            return false;
        return address.matches("(([0-9A-F]){2}:){5}([0-9A-F]){2}");
    }

    /**
     * Check if the key is a Base64 string
     * @param key   Base64 string. This is the key used to authenticate with the IoT Hub
     * @return true if the string is Base64; false otherwise.
     */
    private boolean isBase64(String key){
        try{
            Base64.decode(key, Base64.DEFAULT);
        } catch (IllegalArgumentException e){
            return false;
        }
        return true;
    }

    /**
     * Expected values kid or elderly
     * @param tracker   kid or elderly
     * @return  true if the parameter is kid or elderly; false otherwise;
     */
    private boolean validateTracker(String tracker){
        return tracker.equals("kid") || tracker.equals("elderly");
    }
}
