package com.lifesensor.walltree;

import android.Manifest;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

import static com.lifesensor.walltree.BluetoothClient.REQUEST_ENABLE_BT;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    Button connect;

    EditText remoteAddress;
    EditText sharedAccessKey;
    EditText memberId;
    EditText trackerType;

    TextView localAddress;
    TextView sent;
    TextView received;

    BluetoothClient bluetoothClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.lifesensor.walltree.R.layout.activity_main);

        remoteAddress = findViewById(com.lifesensor.walltree.R.id.remote_address);
        sharedAccessKey = findViewById(com.lifesensor.walltree.R.id.shared_access_key);
        memberId = findViewById(com.lifesensor.walltree.R.id.member_id);
        trackerType = findViewById(com.lifesensor.walltree.R.id.tracker_type);

        localAddress = findViewById(com.lifesensor.walltree.R.id.local_address);
        sent = findViewById(com.lifesensor.walltree.R.id.sent);
        received = findViewById(com.lifesensor.walltree.R.id.received);

        String macAddress = android.provider.Settings.Secure.getString(getContentResolver(), "bluetooth_address");
        localAddress.setText(String.format(Locale.ENGLISH, "Local address: %s", macAddress));

        IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
        registerReceiver(receiver, filter);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if(checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED){
                requestPermissions(
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 100
                );
            }
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == 100 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
            /// enableBluetooth();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult: requestCode: " + requestCode);
        if(requestCode == REQUEST_ENABLE_BT){
            if(resultCode != RESULT_OK){
                Toast.makeText(this, "Impossibile continuare: attivare i bluetooth", Toast.LENGTH_LONG).show();
            } else {
                bluetoothClient.startDiscovery();
                bluetoothClient.list();
            }
        } else if(requestCode == 200){
            if(resultCode != RESULT_OK)
                Toast.makeText(this, "Impossibile continuare: attivare i bluetooth", Toast.LENGTH_LONG).show();
        }
    }

    // Create a BroadcastReceiver for ACTION_FOUND.
    private final BroadcastReceiver receiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)) {
                // Discovery has found a device. Get the BluetoothDevice
                // object and its info from the Intent.
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                String deviceName = device.getName();
                String deviceHardwareAddress = device.getAddress(); // MAC

                Log.d(TAG, String.format("onReceive: hw: %s; nm: %s", deviceHardwareAddress, deviceName));
                if(deviceName != null && deviceName.equals(macAddress)){
                    if(bluetoothClient != null) {
                        bluetoothClient.stopDiscovery();
                        bluetoothClient.connect(deviceHardwareAddress, accessKey, member, tracker);
                    }

                }
            } else if(BluetoothDevice.ACTION_UUID.equals(action)){
                Log.d(TAG, String.format("onReceive: %s", BluetoothDevice.ACTION_UUID));
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //you need this
        if(bluetoothClient != null)
            bluetoothClient.closeAll();
    }

    private String macAddress;
    private String accessKey;
    private String member;
    private String tracker;

    public void sendData(View view) {
        macAddress = remoteAddress.getText().toString().toUpperCase();
        if(macAddress.length() == 0){
            Toast.makeText(this, "Remote address required", Toast.LENGTH_SHORT).show();
            return;
        }

        accessKey = sharedAccessKey.getText().toString();
        if(accessKey.length() == 0){
            Toast.makeText(this, "SharedAccessKey required", Toast.LENGTH_SHORT).show();
            return;
        }

        member = memberId.getText().toString();
        if(member.length() == 0){
            Toast.makeText(this, "MemberId required", Toast.LENGTH_SHORT).show();
            return;
        }

        tracker = trackerType.getText().toString().toLowerCase();
        if(!tracker.equals("kid") && !tracker.equals("elderly")){
            Toast.makeText(this, "Only accepted values: kid or elderly", Toast.LENGTH_SHORT).show();
            return;
        }

        //you need this
        enableBluetooth();
    }

    private void enableBluetooth(){
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if(locationManager != null){
            if(!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
                startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS), 200);
                return;
            }
        }
        bluetoothClient = new BluetoothClient(this);
        if (!bluetoothClient.startDiscovery()) {
            Log.e(TAG, "onRequestPermissionsResult: attivare bluetooth");
        } else {
            bluetoothClient.list();
        }
    }
}
